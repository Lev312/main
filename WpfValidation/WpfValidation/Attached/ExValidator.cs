﻿using System.Windows;
using System.Windows.Controls;

namespace WpfValidation.Attached
{
    public static class ExValidator
    {
        public static readonly DependencyProperty TextProperty = DependencyProperty.RegisterAttached("Text",
            typeof(string), typeof(ExValidator), new UIPropertyMetadata(null, TextPropertyChangedCallback));

        public static readonly DependencyProperty IsShowErrorMessageProperty = DependencyProperty.RegisterAttached(
            "IsShowErrorMessage",
            typeof(bool), typeof(ExValidator), new UIPropertyMetadata(false));

        public static readonly DependencyProperty ErrorMessageProperty = DependencyProperty.RegisterAttached(
            "ErrorMessage", typeof(string), typeof(ExValidator), new PropertyMetadata(null));
        
        public static void SetText(DependencyObject element, string value)
        {
            element.SetValue(TextProperty, value);
        }

        public static string GetText(DependencyObject element)
        {
            return element.GetValue(TextProperty).ToString();
        }

        public static void SetIsShowErrorMessage(DependencyObject element, bool value)
        {
            element.SetValue(IsShowErrorMessageProperty, value);
        }

        public static bool GetIsShowErrorMessage(DependencyObject element)
        {
            return (bool) element.GetValue(IsShowErrorMessageProperty);
        }

        public static void SetErrorMessage(DependencyObject element, string value)
        {
            element.SetValue(ErrorMessageProperty, value);
        }

        public static string GetErrorMessage(DependencyObject element)
        {
            return (string) element.GetValue(ErrorMessageProperty);
        }

        private static void TextPropertyChangedCallback(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs e)
        {
            var passwordBox = dependencyObject as PasswordBox;
            if (passwordBox == null || passwordBox.Password == e.NewValue?.ToString())
            {
                return;
            }

            // ReSharper disable once AssignNullToNotNullAttribute
            passwordBox.Password = e.NewValue?.ToString();
        }
    }
}