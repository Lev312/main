﻿using WpfValidation.Base;
using WpfValidation.Validation;

namespace WpfValidation.Login
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly ApiClient _apiClient;

        public ValidationString Email { get; }
        public ValidationString Password { get; }
        public Command LoginCommand { get; }

        public LoginViewModel()
        {
            _apiClient = new ApiClient();
            Email = new ValidationString(nameof(Email));
            Password = new ValidationString(nameof(Password));
            LoginCommand = new Command(LoginExecute);
            ConfigureValidation();
        }
        
        private void LoginExecute(object arg)
        {
            if (Validate() == false)
            {
                return;
            }
            if (_apiClient.Login(Email.Value, Password.Value))
            {
                //TODO SuccessLogin();
            }
            else
            {
                Email.SetError("Can not login.");
            }

        }

        private void ConfigureValidation()
        {
            ValidationObjects.Add(Email);
            ValidationObjects.Add(Password);
            Validator.AddRule(new EmailRule(() => Email));
            Validator.AddRule(new CustomRule<ValidationString>(() => Password, PasswordRequiredRule, "Password required"));
            Validator.AddRule(new CustomRule<ValidationString>(() => Password, PasswordLengthRule, "Password length should be not less than 5"));
        }

        private bool PasswordRequiredRule()
        {
            return !string.IsNullOrWhiteSpace(Password.Value);
        }

        private bool PasswordLengthRule()
        {
            return Password.Value?.Length > 4;
        }
    }
}
