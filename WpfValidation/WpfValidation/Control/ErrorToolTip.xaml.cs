﻿using System.Windows;
using System.Windows.Input;

namespace WpfValidation.Control
{
    public partial class ErrorToolTip
    {
        public static readonly DependencyProperty ErrorMessageProperty = DependencyProperty.Register(
            "ErrorMessage", typeof(string), typeof(ErrorToolTip), new PropertyMetadata(default(string)));

        public string ErrorMessage
        {
            get => (string) GetValue(ErrorMessageProperty);
            set => SetValue(ErrorMessageProperty, value);
        }
        
        public static readonly DependencyProperty ShowErrorProperty = DependencyProperty.Register(
            "ShowError", typeof(bool), typeof(ErrorToolTip),
            new PropertyMetadata(default(bool), ShowErrorPropertyChangedCallback));


        public bool ShowError
        {
            get => (bool) GetValue(ShowErrorProperty);
            set => SetValue(ShowErrorProperty, value);
        }

        public ErrorToolTip()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var window = Window.GetWindow(this);
            if (window != null)
            {
                window.KeyDown += WindowOnKeyDown;
            }
        }

        private void WindowOnKeyDown(object sender, KeyEventArgs e)
        {
            if (Popup.IsOpen && (e.Key == Key.Enter || e.Key == Key.Tab))
            {
                Popup.IsOpen = false;
            }
        }

        private static void ShowErrorPropertyChangedCallback(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs e)
        {
            var errorToolTip = (ErrorToolTip) dependencyObject;
            var showError = (bool) e.NewValue;
            var hasErrorText = !string.IsNullOrEmpty(errorToolTip.ErrorMessage);
            errorToolTip.Popup.IsOpen = showError && hasErrorText;
        }

        private void PopupGrid_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Popup.IsOpen)
            {
                Popup.IsOpen = false;
            }
        }
    }
}
