﻿using System.Collections.Generic;
using System.Linq;
using WpfValidation.Base;

namespace WpfValidation.Validation
{
    public abstract class ViewModelBase : ObservableObject
    {
        private IValidator _validator;

        protected internal IValidator Validator => _validator ?? (_validator = new Validator());
        protected List<ValidationObject> ValidationObjects { get; } = new List<ValidationObject>();

        protected bool Validate()
        {
            Validator.IsEnabled = true;

            foreach (var validationObject in ValidationObjects)
            {
                validationObject.ResetValidation();
            }
            var isValid = true;
            foreach (var validationObject in ValidationObjects)
            {
                if (!Validator.IsValid(validationObject.PropertyName))
                {
                    var notValidRule = Validator.GetNotValidRules(validationObject.PropertyName).FirstOrDefault();
                    validationObject.SetError(notValidRule?.ErrorMessage);
                    isValid = false;
                }
            }
            return isValid;
        }
    }
}
