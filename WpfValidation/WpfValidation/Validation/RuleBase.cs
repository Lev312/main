﻿using System;
using System.Linq.Expressions;

namespace WpfValidation.Validation
{
    public abstract class RuleBase<T> : IRule
    {
        public abstract bool IsValid { get; }
        public string PropertyName { get; }
        public string ErrorMessage { get; protected set; }

        protected Expression<Func<T>> Expression;

        protected RuleBase(Expression<Func<T>> expression)
        {
            Expression = expression;
            PropertyName = ((MemberExpression) expression.Body).Member.Name;
        }
    }
}
