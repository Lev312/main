﻿namespace WpfValidation.Validation
{
    public interface IRule
    {
        bool IsValid { get; }
        string PropertyName { get; }
        string ErrorMessage { get; }
    }
}
