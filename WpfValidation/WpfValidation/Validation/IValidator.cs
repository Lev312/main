﻿using System;
using System.Linq.Expressions;

namespace WpfValidation.Validation
{
    public interface IValidator
    {
        bool IsEnabled { get; set; }
        void AddRule(IRule rule);
        bool IsValid();
        bool IsValid(string propertyName);
        bool IsValid<T>(Expression<Func<T>> expression);
        IRule[] GetNotValidRules(string propertyName);
        IRule[] GetNotValidRules<T>(Expression<Func<T>> expression);
    }
}
