﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using WpfValidation.Base;

namespace WpfValidation.Validation
{
    public class EmailRule : RuleBase<ValidationString>
    {
        private readonly Expression<Func<ValidationString>> _expression;

        public EmailRule(Expression<Func<ValidationString>> expression) : base(expression)
        {
            _expression = expression;
            ErrorMessage = "Email pattern does not match";
        }

        public override bool IsValid
        {
            get
            {
                var text = _expression.Compile().Invoke()?.Value ?? string.Empty;
                var regex = new Regex(Constants.EmailRegexp);
                var match = regex.Match(text);
                return match.Success;
            }
        }
    }
}
