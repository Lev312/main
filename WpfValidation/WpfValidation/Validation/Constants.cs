﻿namespace WpfValidation.Validation
{
    public static class Constants
    {
        public static readonly string EmailRegexp = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
    }
}
