﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace WpfValidation.Validation
{
    public sealed class Validator : IValidator
    {
        private readonly List<IRule> _rules;
        public bool IsEnabled { get; set; }

        public Validator()
        {
            _rules = new List<IRule>();
        }

        public void AddRule(IRule rule)
        {
            _rules.Add(rule);
        }

        public bool IsValid()
        {
            if (!IsEnabled)
            {
                return true;
            }
            return _rules.All(r => r.IsValid);
        }

        public bool IsValid(string propertyName)
        {
            if (!IsEnabled)
            {
                return true;
            }
            return _rules.Where(r => r.PropertyName == propertyName)
                .All(r => r.IsValid);
        }

        public bool IsValid<T>(Expression<Func<T>> expression)
        {
            var propertyName = ((MemberExpression) expression.Body).Member.Name;
            return IsValid(propertyName);
        }

        public IRule[] GetNotValidRules(string propertyName)
        {
            var rule = _rules.Where(r => r.PropertyName == propertyName && !r.IsValid);
            return rule.ToArray();
        }

        public IRule[] GetNotValidRules<T>(Expression<Func<T>> expression)
        {
            var propertyName = ((MemberExpression) expression.Body).Member.Name;
            return GetNotValidRules(propertyName);
        }
    }
}
