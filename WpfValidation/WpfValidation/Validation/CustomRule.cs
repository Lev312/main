﻿using System;
using System.Linq.Expressions;

namespace WpfValidation.Validation
{
    public class CustomRule<T> : RuleBase<T>
    {
        private readonly Func<bool> _ruleFunc;

        public CustomRule(Expression<Func<T>> expression, Func<bool> ruleFunc) :
            base(expression)
        {
            _ruleFunc = ruleFunc;
        }

        public CustomRule(Expression<Func<T>> expression, Func<bool> ruleFunc, string errorMessage) :
            this(expression, ruleFunc)
        {
            ErrorMessage = errorMessage;
        }

        public override bool IsValid => _ruleFunc?.Invoke() == true;
    }
}
