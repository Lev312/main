﻿using System;
using System.Windows.Input;

namespace WpfValidation.Base
{
    public class Command : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly Action<object> _methodToExecute;
        private readonly Func<bool> _canExecuteEvaluator;

        public Command(Action<object> methodToExecute)
        {
            _methodToExecute = methodToExecute;
        }

        public Command(Action<object> methodToExecute, Func<bool> canExecuteEvaluator)
            : this(methodToExecute)
        {
            _canExecuteEvaluator = canExecuteEvaluator;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecuteEvaluator == null || _canExecuteEvaluator.Invoke();
        }

        public void Execute(object parameter)
        {
            _methodToExecute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
