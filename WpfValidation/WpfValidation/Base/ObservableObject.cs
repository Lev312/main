﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace WpfValidation.Base
{
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void RaisePropertyChanged<T>(Expression<Func<T>> expression)
        {
            RaisePropertyChanged(GetPropertyname(expression));
        }

        protected bool SetProperty<T>(ref T backingField, T value, [CallerMemberName] string propertyName = null)
        {
            var changed = !EqualityComparer<T>.Default.Equals(backingField, value);

            if (changed)
            {
                backingField = value;
                RaisePropertyChanged(propertyName);
            }

            return changed;
        }

        protected bool SetPropertyWithHandler<T>(ref T backingField, T value, PropertyChangedEventHandler handler,
            [CallerMemberName] string propertyName = null) where T : INotifyPropertyChanged
        {
            if (backingField != null)
            {
                backingField.PropertyChanged -= handler;
            }

            if (value != null)
            {
                value.PropertyChanged += handler;
            }

            return SetProperty(ref backingField, value, propertyName);
        }
        protected string GetPropertyname<T>(Expression<Func<T>> expression)
        {
            var propertyName = ((MemberExpression)expression.Body).Member.Name;
            return propertyName;
        }
    }
}
