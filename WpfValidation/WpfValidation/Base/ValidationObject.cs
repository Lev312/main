﻿using System;

namespace WpfValidation.Base
{
    public class ValidationObject : ObservableObject
    {
        private string _errorMessage;
        private bool _hasError;

        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                if (HasError)
                {
                    HasError = false; // specific reset for XAML popup behavior
                }
                SetProperty(ref _errorMessage, value);
            }
        }

        public bool HasError
        {
            get => _hasError;
            set
            {
                if (value)
                {
                    SetProperty(ref _hasError, false); // specific reset for XAML popup behavior
                }
                SetProperty(ref _hasError, value);
            }
        }

        public string PropertyName { get; }

        public ValidationObject(string propertyname)
        {
            PropertyName = propertyname;
        }

        public void SetError(string errorMessage)
        {
            ErrorMessage = errorMessage;
            HasError = true;
        }

        public void ResetValidation()
        {
            ErrorMessage = null;
            HasError = false;
        }
    }

    public class ValidationObject<T> : ValidationObject
    {
        private T _value;

        public T Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }

        public event EventHandler ValueChanged;

        protected ValidationObject(string propertyname) : base(propertyname)
        {
        }

        protected override void RaisePropertyChanged(string propertyName = null)
        {
            base.RaisePropertyChanged(propertyName);
            if (propertyName == nameof(Value))
            {
                ValueChanged?.Invoke(this, EventArgs.Empty);
            }
        }
    }

    public class ValidationString : ValidationObject<string>
    {
        public ValidationString(string propertyname) : base(propertyname)
        {
            Value = string.Empty;
        }
    }
}
