﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using WpfValidation.Attached;

namespace WpfValidation.Behaviors
{
    public class PasswordBoxBindingBehavior : Behavior<PasswordBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.PasswordChanged += OnPasswordBoxValueChanged;
        }

        private void OnPasswordBoxValueChanged(object sender, RoutedEventArgs e)
        {
            AssociatedObject.SetValue(ExValidator.TextProperty, AssociatedObject.Password);
        }
    }
}