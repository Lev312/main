﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WpfValidation.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isVisible = (bool?) value;
            bool.TryParse(parameter?.ToString(), out var isReverse);
            var visibility = isVisible.HasValue && isVisible.Value == !isReverse
                ? Visibility.Visible
                : Visibility.Collapsed;
            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (Visibility)value;
            return visibility == Visibility.Visible;
        }
    }
}
