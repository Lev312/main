﻿using System.Windows;
using WpfLocalization.Services;
using WpfLocalization.Views.Windows;

namespace WpfLocalization.Client
{
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            CultureService.Instance.ApplyCulture();

            var launcherWindow = new LauncherLauncherWindow();
            launcherWindow.Show();
        }
    }
}
