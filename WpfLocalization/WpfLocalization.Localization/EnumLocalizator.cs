﻿using System;
using System.Collections.Generic;
using WpfLocalization.Models;

namespace WpfLocalization.Localization
{
    public class EnumLocalizator
    {
        static Dictionary<Type, Dictionary<object, string>> _dictionary;

        static EnumLocalizator()
        {
            ConfigureEnums();
        }

        public string Localize(object enumValue)
        {
            var hasValue = _dictionary.ContainsKey(enumValue.GetType()) &&
                           _dictionary[enumValue.GetType()].ContainsKey(enumValue);

            return hasValue ? _dictionary[enumValue.GetType()][enumValue] : null;
        }

        private static void ConfigureEnums()
        {
            _dictionary = new Dictionary<Type, Dictionary<object, string>>
            {
                {
                    typeof(Levels), new Dictionary<object, string>
                    {
                        {Levels.Low, Text.Enum_Level_Low},
                        {Levels.Medium, Text.Enum_Level_Medium},
                        {Levels.Hight, Text.Enum_Level_Hight},
                    }
                }
            };
        }
    }
}
