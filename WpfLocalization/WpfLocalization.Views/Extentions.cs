﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace WpfLocalization.Views
{
    public static class Extentions
    {
        public static void UpdateAllBindings<TBinding>(this DependencyObject element) where TBinding : Binding
        {
            //Immediate Properties
            List<FieldInfo> propertiesAll = new List<FieldInfo>();
            Type currentLevel = element.GetType();
            while (currentLevel != typeof(object))
            {
                propertiesAll.AddRange(currentLevel.GetFields());
                currentLevel = currentLevel.BaseType;
            }
            var propertyFields = propertiesAll.Where(p => p.FieldType == typeof(DependencyProperty));
            var dependencyProperties = propertyFields.Select(f => f.GetValue(element)).OfType<DependencyProperty>();
            var bindingExpressions = dependencyProperties
                .Select(dp => BindingOperations.GetBindingExpression(element, dp))
                .Where(be => be != null)
                .ToArray();
            foreach (var bindingExpression in bindingExpressions)
            {
                if (bindingExpression?.ParentBinding is TBinding)
                {
                    bindingExpression.UpdateTarget();
                }
            }

            //Children
            int childrenCount = VisualTreeHelper.GetChildrenCount(element);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(element, i);
                child.UpdateAllBindings<TBinding>();
            }
        }
    }
}
