﻿using System;
using System.Globalization;
using System.Windows.Data;
using WpfLocalization.Localization;

namespace WpfLocalization.Views.Converters
{
    public class EnumConverter:IValueConverter
    {
        private readonly EnumLocalizator _enumLocalizator;

        public EnumConverter()
        {
            _enumLocalizator = new EnumLocalizator();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return _enumLocalizator.Localize(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
