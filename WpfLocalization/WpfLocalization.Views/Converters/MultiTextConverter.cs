﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace WpfLocalization.Views.Converters
{
    public class MultiTextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var text = values[0]?.ToString() ?? string.Empty;
            var args = values.Skip(1).ToArray();
            return string.Format(text, args);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
