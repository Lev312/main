﻿using System.Windows;
using WpfLocalization.ViewModels.Main;

namespace WpfLocalization.Views.Windows
{

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
    }
}
