﻿using System.Windows;
using WpfLocalization.ViewModels.Launcher;
using WpfLocalization.Views.Bindings;
using WpfLocalization.Views.Interfaces;

namespace WpfLocalization.Views.Windows
{
    public partial class LauncherLauncherWindow : Window, ILauncherWindow
    {
        public LauncherLauncherWindow()
        {
            InitializeComponent();
            DataContext = new LauncherViewModel(this);
        }

        public void UpdateLocalization()
        {
            this.UpdateAllBindings<TextBinding>();
        }

        public void Continue()
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }
    }
}
