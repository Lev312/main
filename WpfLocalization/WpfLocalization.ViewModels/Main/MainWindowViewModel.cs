﻿using System;
using System.Linq;
using WpfLocalization.Models;
using WpfLocalization.ViewModels.Base;

namespace WpfLocalization.ViewModels.Main
{
    public class MainWindowViewModel : ObservableObject
    {
        private Levels _selectedLevel;

        public Levels SelectedLevel
        {
            get => _selectedLevel;
            set => SetProperty(ref _selectedLevel, value);
        }

        public Levels[] Levels { get; }

        public MainWindowViewModel()
        {
            Levels = Enum.GetValues(typeof(Levels)).Cast<Levels>().ToArray();
        }
    }
}
