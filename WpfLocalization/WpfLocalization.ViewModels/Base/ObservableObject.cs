﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfLocalization.ViewModels.Base
{
    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T origValue, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (Equals(origValue, newValue))
            {
                return false;
            }
            origValue = newValue;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}
