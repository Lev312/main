﻿using System.Windows.Input;
using WpfLocalization.Services;
using WpfLocalization.ViewModels.Base;
using WpfLocalization.ViewModels.Commands;
using WpfLocalization.Views.Interfaces;

namespace WpfLocalization.ViewModels.Launcher
{
    public class LauncherViewModel:ObservableObject
    {
        private readonly CultureService _cultureService;
        private readonly ILauncherWindow _launcherLauncherWindow;

        public string[] Cultures => _cultureService.Cultures;

        private string _selectedCulture;

        public string SelectedCulture
        {
            get => _selectedCulture;
            set
            {
                SetProperty(ref _selectedCulture, value);
                _cultureService.ApplyCulture(_selectedCulture);
                _launcherLauncherWindow.UpdateLocalization();
            }
        }

        public ICommand ContinueCommand { get; }
        
        public LauncherViewModel(ILauncherWindow launcherLauncherWindow)
        {
            _launcherLauncherWindow = launcherLauncherWindow;
            _cultureService = CultureService.Instance;
            ContinueCommand = new RelayCommand(ContinueExecute);
            _selectedCulture = _cultureService.GetCurrentCultureName();
        }

        private void ContinueExecute(object parameter)
        {
            _cultureService.SaveCulture(SelectedCulture);
            _launcherLauncherWindow.Continue();
        }
    }
}
