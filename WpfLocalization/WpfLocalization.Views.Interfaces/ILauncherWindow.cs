﻿namespace WpfLocalization.Views.Interfaces
{
    public interface ILauncherWindow
    {
        void Continue();
        void UpdateLocalization();
    }
}
