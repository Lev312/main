﻿using System;
using Microsoft.Win32;

namespace WpfLocalization.Services
{
    public class ApplicationRegistryService
    {
        private static readonly string KeyPath;

        static ApplicationRegistryService()
        {
            KeyPath = Environment.Is64BitOperatingSystem
                ? "Software\\Wow6432Node\\WpfLocalization"
                : "Software\\WpfLocalization";
        }
        public void SaveValue(string name, string value)
        {
            using (var key = Registry.LocalMachine.CreateSubKey(KeyPath))
            {
                key.SetValue(name, value);
            }
        }
        public string GetValue(string name)
        {
            using (var key = Registry.LocalMachine.OpenSubKey(KeyPath))
            {
                return key?.GetValue(name) as string;
            }
        }
    }
}
