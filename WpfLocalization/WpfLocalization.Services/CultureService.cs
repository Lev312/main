﻿using System.Globalization;

namespace WpfLocalization.Services
{
    public class CultureService
    {
        private static readonly string CultureRegistryKey = "CultureName";


        private readonly ApplicationRegistryService _registryService;

        public string[] Cultures { get; } = {"en-US", "uk-UA"};

        private static CultureService _cultureService;
        public static CultureService Instance => _cultureService ?? (_cultureService = new CultureService());

        private CultureService()
        {
            _registryService = new ApplicationRegistryService();
        }

        public string GetCurrentCultureName()
        {
            var culture = _registryService.GetValue(CultureRegistryKey);
            return culture ?? Cultures[0];
        }

        public void SaveCulture(string culture)
        {
            _registryService.SaveValue(CultureRegistryKey, culture);
        }

        public void ApplyCulture(string cultureName = null)
        {
            if (cultureName == null)
            {
                cultureName = GetCurrentCultureName();
            }
            var cultureInfo = new CultureInfo(cultureName);
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
        }
    }
}
